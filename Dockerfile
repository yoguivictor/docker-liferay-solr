# liferay/solr:4.7.0
FROM liferay/base:6.2

ENV TOMCAT_HOME /opt/apache-tomcat-7.0.42
ENV SOLR_HOME /opt/solr-4.7.0/example/solr

RUN yum -y install tar && yum clean all

RUN wget https://archive.apache.org/dist/tomcat/tomcat-7/v7.0.42/bin/apache-tomcat-7.0.42.tar.gz -qO- | tar xz -C /opt
RUN wget https://archive.apache.org/dist/lucene/solr/4.7.0/solr-4.7.0.tgz -qO- | tar xz -C /opt 

RUN cp /opt/solr-4.7.0/dist/solr-4.7.0.war $TOMCAT_HOME/webapps/
RUN cp /opt/solr-4.7.0/dist/solrj-lib/* $TOMCAT_HOME/lib/ 

COPY setup/server.xml $TOMCAT_HOME/conf/
COPY setup/setenv.sh  $TOMCAT_HOME/bin/
COPY setup/schema.xml $SOLR_HOME/
COPY setup/schema.xml $SOLR_HOME/collection1/conf/

CMD $TOMCAT_HOME/bin/catalina.sh run

